---
layout: markdown_page
title: "Business Operations"
---
<a name="topofpage"></a>
## On this page
{:.no_toc}

- TOC
{:toc}  


## In this section
- [Customer Lifecycle](/handbook/business-ops/customer-lifecycle)
- [Database Management](/handbook/business-ops/database-management)
     - [Tech Stack Details](/handbook/business-ops/tech-stack/)
- [Reporting](/handbook/business-ops/reporting)



## Purpose   
Overview of systems, workflows and processes for the three functional groups - [Sales](/handbook/sales), [Marketing](/handbook/marketing) and [Customer Success](/handbook/customer-success/) - that work closely together. This section is a singular reference point for operational management of the database and where the most up-to-date workflows, routing, rules and definition sets are managed.  Tracking all tools, license allocation, admins and contracts for the combined Marketing & Sales Operations tech stack. Tips, tricks and how to's for the various applications and external resources that will help user be more productive in the business systems.    


## Reaching the Teams (internally)   
- **Public Issue Tracker**: A short list of the main, public issue trackers. For respective teams, there may be additional resources. Please use confidential issues for topics that should only be visible to team members at GitLab
    - [Sales](https://gitlab.com/gitlab-com/sales/issues/) - general sales related needs & issues
    - [Salesforce](https://gitlab.com/gitlab-com/salesforce/issues) - Salesforce specific needs, issues and questions
    - [Marketing](https://gitlab.com/gitlab-com/marketing/issues) - All issues related to website, product, design, events, webcasts, lead routing, social media and community relations
    - [Customer Success SA Service Desk](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/issues) - your go to place for pre-sales, post-sales, customer training and professional services
- **Email**: Please use the 'Email, Slack, and GitLab Groups and Aliases' document for the appropriate team alias.  
- **Slack**: A short list of the primary Slack channels used by these respective teams  
    - `#sales`
    - `#marketing`
    - `#solutions_architect`
    - `#bdr-team`
    - `#sdr`
    - `#sfdc-users`
    - `#lead-questions`


## Tech Stack     

<div class="tg-wrap"><table class="tg">
  <tr>
    <th class="tg-k6pi">Applications & Tools</th>
    <th class="tg-031e">Who Should Have Access</th>
    <th class="tg-yw4l">Whom to Contact w/Questions</th>
    <th class="tg-yw4l">Admin(s)</th>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#clearbit">Clearbit</a></strong></td>
    <td>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR</td>
    <td><strong>JJ Cordz (primary)</strong><br>Francis Aquino</td>
    <td>JJ Cordz<br>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#discoverorg">DiscoverOrg</a></strong></td>
    <td>Mid-Market AEs<br>Outbound SDR</td>
    <td><strong>Francis Aquino</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#dogood">DoGood</a></strong></td>
    <td>Online Marketing<br>SDR Team Lead</td>
    <td><strong>Nicholas Flynn</strong></td>
    <td>Nicholas Flynn<br>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong>Drift</strong></td>
    <td>Inbound BDR</td>
    <td><strong>Molly Young (primary)</strong><br>JJ Cordz (integration)</td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong>FunnelCake</strong></td>
    <td>Sr Director Demand Gen<br>Marketing OPS<br>Online Marketing</td>
    <td></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#google-analytics">Google Analytics</a></strong></td>
    <td>Sr Director Demand Gen<br>Marketing OPS<br>Online Marketing<br>Product Marketing<br>Content Marketing</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#google-tag-manager">Google Tag Manager</a></strong></td>
    <td>Online Marketing</td>
    <td></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong>GovWin IQ</strong></td>
    <td>Sales - Federal</td>
    <td><strong>Francis Aquino</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong>InsightSquared</strong></td>
    <td>Executives<br>Sales Leadership</td>
    <td><strong>Francis Aquino</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#leandata">LeanData</a></strong></td>
    <td>Marketing OPS<br>Sales OPS</td>
    <td><strong>JJ Cordz (primary)</strong><br>Francis Aquino</td>
    <td>JJ Cordz<br>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong>LicenseApp</strong></td>
    <td>Access via GitLab credentials</td>
    <td><strong>Francis Aquino</strong></td>
    <td></td>
  </tr>
  <tr>
    <td><strong>LinkedIn Sales Navigator</strong></td>
    <td>Sales Leadership<br>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#marketo">Marketo</a></strong></td>
    <td>Marketing OPS<br>Online Marketing<br>Content Marketing<br>Field Marketing</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#optimizely">Optimizely</a></strong></td>
    <td>Online Marketing</td>
    <td></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#outreachio">Outreach.io</a></strong></td>
    <td>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR</td>
    <td><strong>Francis Aquino (primary)</strong><br>JJ Cordz<br>Chet Backman</td>
    <td>Francis Aquino<br>JJ Cordz<br>Chet Backman</td>
  </tr>
  <tr>
    <td><strong>Piwik</strong></td>
    <td>Online Marketing</td>
    <td></td>
    <td>Online Marketing Manager</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#salesforce">Salesforce</a></strong></td>
    <td>Sales Leadership<br>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR<br>Solutions Architects<br>Marketing OPS</td>
    <td>Not related to quotes or lead routing: <strong>Francis Aquino</strong><br><br>Lead Routing/Scoring/Source: <strong>JJ Cordz</strong></td>
    <td>Francis Aquino<br>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#sertifi">Sertifi</a></strong></td>
    <td>Account Executives<br>Account Managers</td>
    <td><strong>Francis Aquino</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#terminus">Terminus</a></strong></td>
    <td>Online Marketing</td>
    <td><strong>Emily Kyle (primary)</strong><br>JJ Cordz</td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#unbounce">Unbounce</a></strong></td>
    <td>Marketing OPS<br>Online Marketing<br>Content Marketing<br>Field Marketing<br>Design</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong>WebEx</strong></td>
    <td>Marketing OPS</td>
    <td><strong>JJ Cordz</strong></td>
    <td>JJ Cordz</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#zendesk">Zendesk</a></strong></td>
    <td>Access via Salesforce</td>
    <td><strong>Lee Matos</strong></td>
    <td>Francis Aquino</td>
  </tr>
  <tr>
    <td><strong>Zoom</strong></td>
    <td>Access via GitLab credentials</td>
    <td><strong>PeopleOPS</strong></td>
    <td>PeopleOPS</td>
  </tr>
  <tr>
    <td><strong><a href="/handbook/business-ops/tech-stack/#zuora">Zuora</a></strong></td>
    <td>Access via Salesforce</td>
    <td><strong>Wilson Lau (primary)</strong><br>Francis Aquino (secondary)</td>
    <td>Francis Aquino</td>
  </tr>
</table></div>

[Operations License Tracking & Contract Details](https://docs.google.com/a/gitlab.com/spreadsheets/d/1-W0rmZalDOpTNQf4K50DZ1y3slgmNSrPVPuF6_Hfz6s/edit?usp=sharing) (document can only be accessed by GitLab team members)



## Glossary   

| Term | Definition |
| :--- | :--- |
| Accepted Lead | A lead a Sales Development Representative or Business Development Represenative agrees to work until qualified in or qualified out |
| Account | An organization tracked in salesforce.com. An account can be a prospect, customer, former customer, integrator, reseller, or prospective reseller |
| AM | Account Manager |
| AE | Account Executive |
| APAC | Asia-Pacific |
| BDR | Business Development Representative |
| CS | Customer Success |
| EMEA | Europe, Middle East and Asia |
| Inquiry | an Inbound request or response to an outbound marketing effort |
| IQM | Initial Qualifying Meeting |
| MQL | Marketo Qualified Lead - an inquiry that has been qualified through systematic means (e.g. through demographic/firmographic/behaviour lead scoring) |
| NCSA | North, Central, South America |
| Qualified Lead | A lead a Sales Development Representative or Business Development Representative has qualified, converted to an opportunity and assigned to a Sales Representative (Stage `0-Pending Accpetance`) |
| RD | Regional Director |
| Sales Admin | Sales Administrator |
| [SAO] | Sales Accepted Opportunity - an opportunity Sales agrees to pursue following an Inital Qualifying Meeting |
| SDR | Sales Development Representative |
| SLA | Service Level Agreement |
| SCLAU | Abbreviation for SAO (Sales Accepted Opportunity) Count Large and Up |
| SQO | Sales Qualified Opportunity |
| TEDD | Technology, Engineering, Development and Design - used to estimate the maximum potential users of GitLab at a company |
| Won Opportunity | Contract signed to Purchase GitLab |      


## Other Related Pages

- [SDR Handbook](/handbook/marketing/marketing-sales-development/sdr/)
- [Marketing Operations](/handbook/marketing/marketing-sales-development/marketing-operations/)   
- [Customer Success](/handbook/customer-success/)





[Return to Top of Page](#topofpage)

[SAO]: /handbook/marketing/marketing-sales-development/sdr/#criteria-for-sales-accepted-opportunity-sao
